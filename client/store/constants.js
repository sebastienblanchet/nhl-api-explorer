import {
  getField,
  updateField
} from "vuex-map-fields"

export const namespaced = true;

export const state = () => ({
  root: "https://statsapi.web.nhl.com",
  api: "/api/v1/"
});

export const getters = {
  getField,
};

export const mutations = {
  updateField,
};

export const actions = {

};