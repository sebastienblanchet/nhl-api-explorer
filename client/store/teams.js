import {
  getField,
  updateField
} from "vuex-map-fields"

export const namespaced = true;

export const state = () => ({
  list: []
});

export const getters = {
  getField,
};

export const mutations = {
  updateField,
};

export const actions = {

};
