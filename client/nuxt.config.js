import colors from 'vuetify/es5/util/colors'

export default {
  ssr: false,

  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: 'nhl api explorer - %s',
    title: process.env.npm_package_name || '',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },

  css: [
    'vue-json-pretty/lib/styles.css',
    '@koumoul/vjsf/dist/main.css'
  ],

  plugins: [
    '@/plugins/pretty',
    '@/plugins/vjsf',
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy'
  ],

  axios: {
    // in dev mode, go through proxy to talk on node server on seperate port
    baseURL: process.env.NODE_ENV !== 'production' ? `http://localhost:${process.env.PORT || '3000'}/api` : process.env.HEROKU_BASE
  },

  // proxy will be ignored if prod from axios config above
  proxy: {
    '/api/': {
      target: `http://localhost:${process.env.PORT || '3001'}`,
      pathRewrite: {
        '^/api/': '/'
      }
    },
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    transpile: ['vuetify/lib', /@koumoul/], // necessary for "à la carte" import of vuetify components.
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.md$/,
        loader: "raw-loader"
      })
    }
  }
}