const axios = require('axios');
const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express()

const port = process.env.PORT || 3001;

app.use(cors());
app.use(bodyParser.json());

app.use(express.static('client/dist'));

async function nhlQuery(str) {
  try {
    let url = `https://statsapi.web.nhl.com/api/v1/${str}`;
    console.log(url);
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    // console.error(error);
  }
}

// for vue json forms
// read back, this needs to be GET
app.get('/lists/:query', async (req, res) => {
  try {
    let data = await nhlQuery(req.params.query);
    res.send(data);
  } catch (e) {
    // console.log(e);
    res.sendStatus(400);
  }
})

app.get('/expands/:query', async (req, res) => {
  try {
    let data = await nhlQuery("expands");
    data = data.filter((el) => el.name.includes(req.body.query));
    res.send(data);
  } catch (e) {
    // console.log(e);
    res.sendStatus(400);
  }
})

app.post('/v1', async (req, res) => {
  try {
    const data = await nhlQuery(req.body.query);
    res.send(data);
  } catch (e) {
    // console.log(e);
    res.sendStatus(400);
  }
});

app.listen(port, () => {
  console.log(`Listen on http://localhost:${port}`);
})