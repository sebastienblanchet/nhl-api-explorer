# nhl-api-explorer

[![pipeline status](https://gitlab.com/sebastienblanchet/nhl-api-explorer/badges/master/pipeline.svg)](https://gitlab.com/sebastienblanchet/nhl-api-explorer/-/commits/master)

Live showcase of documented API from [dword4/nhlapi](https://gitlab.com/dword4/nhlapi).

[See Heroku App](https://nhl-api-explorer.herokuapp.com/)